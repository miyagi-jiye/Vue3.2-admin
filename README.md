<p align="center"><img src="https://s1.ax1x.com/2022/07/29/vPnpDJ.png" alt="vPnpDJ.png" border="0" /></p>
<h1 align="center" style="margin: 10px 0 10px; font-weight: bold;">Vue3.2-pinia-elementPlus-vite-admin</h1>
<h4 align="center">基于Vue3开发的电商后台管理系统</h4>
<p align="center">
<img src="https://img.shields.io/badge/Vue-3.2-303643?labelColor=51AF5A&logo=vue.js" alt="Vue"></img>
<img src="https://img.shields.io/badge/Vite-2.9-303643?labelColor=A94EFE&logo=vite" alt="Vite"></img>
<img src="https://img.shields.io/badge/Pinia-2.0-303643?labelColor=FFDB5C" alt="Pinia"></img>
<img src="https://img.shields.io/badge/Vue_Router-4.0-303643?labelColor=41B883" alt="Vue-Router"></img>
<img src="https://img.shields.io/badge/Element_Plus-2.2-303643?labelColor=337ecc" alt="Element-Plus"></img>
<img src="https://img.shields.io/badge/-Less-1D365D?logoColor=white&logo=less" alt="Less"></img>
</p>

## ~~在线体验~~（本人服务器已过期，无法访问接口）


超级管理员
账号：admin
密码：123456

~~Vue2.0版演示地址：[http://miyagi-jiye.gitee.io/vue2-admin](http://miyagi-jiye.gitee.io/vue2-admin)~~  
~~Vue3.2版演示地址：[http://miyagi-jiye.gitee.io/vue3.2-admin](http://miyagi-jiye.gitee.io/vue3.2-admin)~~  
<!-- Vue3.2版演示地址：[http://miyagi-jiye.gitee.io/vite-element-plus-pinia-admin](http://miyagi-jiye.gitee.io/vite-element-plus-pinia-admin)   -->
~~注意：如果无法登录，请查看网址是否是 **http://** 而不是 **https://**~~

## 开发简介

该项目主要采用 `Vue.js`进行开发，先后进行过两次重构

- 初始开发主要使用 `vue-cli`， `Vue2.0 `，`Axios `， `VueRouter `， `Element-UI`组件库，开发完成后发觉当前项目的代码耦合度太高，不便于以后的维护，便开始着手该项目的重构。
- 第一次重构采用 `Vue3.0`写法，并引入了全局状态管理工具 `Vuex`将数据进行抽离并分成相应模块进行管理，极大的降低了该项目代码的耦合度，提高了可维护性和可读性，由于 `Element-UI`不支持 `Vue3`，所以升级使用 `Element-Plus`组件库。
- 第二次重构使用 `vite`进行打包构建，解决了项目热更新时间长的问题，并采用了  `Vue3.2 `的 `<script setup>`语法糖写法，并使用 `Pinia `替换 `Vuex`，不仅减少了重复性代码，还降低了项目的打包体积，更进一步提高了代码的可读性。

## 主要功能

1. 用户管理：用户是系统操作者，该功能主要完成超级管理员对其他用户的配置。
2. 用户列表：展示所有用户列表，实现超级管理员对其他普通用户的配置
3. 角色列表：表格展示已有角色，展开列表可显示对应角色拥有的权限，普通用户只可预览，超级管理员可新增角色并分配权限
4. 权限列表：展示当前用户拥有的所有权限
5. 商品列表：以表格形式展示所有商品详情，可分页预览，以及商品的增删改查
6. 分类参数：可对静态属性和动态参数分别进行增删改查
7. 商品分类：表格可展开以树形结构展示多级分类，可对所有层级分类进行增删改查
8. 订单列表：展示用户订单详情，支付状态，支付方式，物流详情，可对订单进行一定修改
9. 数据报表：数据可视化展示
10. 其他功能：404页面，暗黑模式，移动端简易适配，系统主题配置，全屏功能，语言国际化，主控台

## 使用步骤

### 1.下载依赖

```
npm install
```

### 2.启动项目

```
npm run dev
```

### 3.打包项目

```
npm run build
```

## 演示图

<div style="display: flex;flex-wrap: wrap;flex-direction: row;justify-content: center;gap:20px">
    <!-- <img src="https://s1.ax1x.com/2022/08/08/vQJ939.png" alt="vPaFIK.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaFIK.png" alt="vPaFIK.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaAPO.png" alt="vPaAPO.png" border="0">
    <img src="https://s1.ax1x.com/2022/08/08/vQGam6.png" alt="vPaAPO.png" border="0">
    <img src="https://s1.ax1x.com/2022/08/08/vQGt61.png" alt="vPaAPO.png" border="0">
    <img src="https://s1.ax1x.com/2022/08/08/vQGNOx.png" alt="vPaAPO.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPUXPU.md.png" alt="vPUXPU.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/10/08/xGkyz4.png" alt="vPaEGD.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPUL5T.md.png" alt="vPUL5T.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPUqaV.md.png" alt="vPUqaV.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPUjGF.md.png" alt="vPUjGF.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPUxxJ.md.png" alt="vPUxxJ.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPUv24.md.png" alt="vPUv24.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaSM9.md.png" alt="vPaSM9.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaprR.md.png" alt="vPaprR.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPa9q1.md.png" alt="vPa9q1.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaPVx.md.png" alt="vPaPVx.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaia6.md.png" alt="vPaia6.md.png" border="0">
    <img src="https://s1.ax1x.com/2022/07/29/vPaEGD.md.png" alt="vPaEGD.md.png" border="0"> -->
    <img src="./demo_Image/14.png" alt="演示图" border="0">
    <img src="./demo_Image/1.png" alt="演示图" border="0">
    <img src="./demo_Image/2.png" alt="演示图" border="0">
    <img src="./demo_Image/3.png" alt="演示图" border="0">
    <img src="./demo_Image/4.png" alt="演示图" border="0">
    <img src="./demo_Image/5.png" alt="演示图" border="0">
    <img src="./demo_Image/6.png" alt="演示图" border="0">
    <img src="./demo_Image/7.png" alt="演示图" border="0">
    <img src="./demo_Image/8.png" alt="演示图" border="0">
    <img src="./demo_Image/9.png" alt="演示图" border="0">
    <img src="./demo_Image/10.png" alt="演示图" border="0">
    <img src="./demo_Image/11.png" alt="演示图" border="0">
    <img src="./demo_Image/12.png" alt="演示图" border="0">
    <img src="./demo_Image/13.png" alt="演示图" border="0">

</div>
