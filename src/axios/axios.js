import axios from "axios";

// axios.defaults.headers.post["Content-Type"] =
//   "application/x-www-form-urlencoded";

axios.defaults.baseURL = "http://124.223.49.16:8889/api/private/v1";
// axios.defaults.baseURL = "/api";// 跨域代理写法

// 添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 添加 token 验证的 Authorization 字段
    // 判断后台服务器地址添加不同的请求头
    if (config.baseURL === 'http://124.223.49.16:8889/api/private/v1') {
      config.headers.Authorization = window.sessionStorage.getItem("token");
    }
    // console.log(config);
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    return response;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);

export default axios;
