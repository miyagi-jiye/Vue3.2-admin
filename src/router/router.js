import { createWebHistory, createWebHashHistory, createRouter } from "vue-router";
import NProgress from "nprogress"; //引入nprogress 进度条插件
import "nprogress/nprogress.css"; //引入nprogress 进度条样式文件

const layout = () => import("@/views/Layout/layout.vue");

const routes = [
  // 仪表盘
  {
    path: "/",
    component: layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        meta: { title: "仪表盘", path: "/dashboard" },
        component: () => import("@/views/Dashboard/dashboard.vue"),
        children: [
          {
            path: "dataVisualize",
            name: "DataVisualize",
            meta: { title: "数据可视化", path: "/dashboard/dataVisualize" },
            component: () => import("@/views/Dashboard/dataVisualize/index.vue"),
          },
          {
            path: "embedded",
            name: "Embedded",
            meta: { title: "内嵌页面", path: "/dashboard/embedded" },
            component: () => import("@/views/Dashboard/embedded/index.vue"),
          },
          {
            path: 'animation',
            name: 'animation',
            meta: { title: "动画时间表(跨域)", path: "/dashboard/animation" },
            component: () => import("@/views/Animation/animation.vue"),
          }
        ]
      },
      // 个人信息
      {
        path: "userinfo",
        name: "UserInfo",
        meta: { title: "个人信息", path: "/userinfo" },
        component: () => import("@/views/UserInfo/index.vue"),
      },
    ],
  },
  // 用户管理
  {
    path: "/users",
    name: "Users",
    meta: { title: "用户管理", path: "/users" },
    component: layout,
    redirect: "/users/users", //重定向
    children: [
      {
        path: "users",
        name: "Users",
        meta: { title: "用户列表", path: "/users/users" },
        component: () => import("@/views/Users/Users/users.vue"),
      },
    ],
  },
  // 权限管理
  {
    path: "/rights",
    name: "Rights",
    meta: { title: "权限管理", path: "/rights" },
    component: layout,
    redirect: "/rights/rights", //重定向
    children: [
      {
        path: "roles",
        name: "Roles",
        meta: { title: "角色列表", path: "/rights/roles" },
        component: () => import("@/views/Rights/Roles/roles.vue"),
      },
      {
        path: "rights",
        name: "Rights",
        meta: { title: "权限列表", path: "/rights/rights" },
        component: () => import("@/views/Rights/Rights/rights.vue"),
      },
    ],
  },
  // 商品管理
  {
    path: "/goods",
    name: "Goods",
    meta: { title: "商品管理", path: "/goods" },
    component: layout,
    redirect: "/goods/goods", //重定向
    children: [
      {
        path: "goods",
        name: "Goods",
        meta: { title: "商品列表", path: "/goods/goods" },
        component: () => import("@/views/Goods/Goods/goods.vue"),
        // children: [
        //   {
        //     path: "add",
        //     name: "Add",
        //     meta: { title: "添加商品", path: "/goods/goods/add" },
        //     component: () => import("@/views/Goods/Goods/Add/add.vue")
        //   }
        // ]
      },
      {
        path: "goods/add",
        name: "Add",
        meta: { title: "添加商品", path: "/goods/goods/add" },
        component: () => import("@/views/Goods/Goods/Add/add.vue")
      },
      {
        path: "params",
        name: "Params",
        meta: { title: "分类参数", path: "/goods/params" },
        component: () => import("@/views/Goods/Params/params.vue"),
      },
      {
        path: "categories",
        name: "Categories",
        meta: { title: "商品分类", path: "/goods/categories" },
        component: () => import("@/views/Goods/Categories/categories.vue"),
      },
    ],
  },
  // 订单管理
  {
    path: "/orders",
    name: "Orders",
    meta: { title: "订单管理", path: "/orders" },
    component: layout,
    redirect: "/orders/orders", //重定向
    children: [
      {
        path: "orders",
        name: "Orders",
        meta: { title: "订单列表", path: "/orders/orders" },
        component: () => import("@/views/Orders/Orders/orders.vue"),
      },
    ],
  },
  // 数据统计
  {
    path: "/reports",
    name: "Reports",
    meta: { title: "数据统计", path: "/reports" },
    component: layout,
    redirect: "/reports/reports", //重定向
    children: [
      {
        path: "reports",
        name: "Reports",
        meta: { title: "数据报表", path: "/reports/reports" },
        component: () => import("@/views/Reports/Reports/reports.vue"),
      },
    ],
  },
  //动态匹配 /login/123abc ,?代表可选,例如 /login/ 也可以匹配到，不加?就匹配不到
  {
    path: "/login/:id?",
    name: "Login",
    component: () => import("@/views/Login/login.vue"),
  },
  // 将匹配所有内容并将其放在 `$route.params.pathMatch` 下
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    component: () => import("@/views/404/404.vue"),
  },
  // 将匹配以 `/test` 开头的所有内容，并将其放在 `$route.params.afterUser` 下
  {
    path: "/test:afterUser(.*)",
    name: "Test",
    meta: { title: "测试页面", path: "/test" },
    component: () => import("@/views/Test/test.vue"),
  },
  // 日程待办
  {
    path: "/calendar",
    name: "Calendar",
    meta: { title: "日程待办", path: "/calendar" },
    component: () => import("@/views/Calendar/calendar.vue"),
  },
  // 疫情可视化
  {
    path: "/covid",
    name: "Covid",
    meta: { title: "新冠疫情", path: "/covid" },
    component: () => import("@/views/Covid/covid.vue")
  },
  // 便签
  {
    path: "/notes",
    name: "Notes",
    meta: { title: "便利贴", path: "/notes" },
    component: () => import("@/views/Notes/notes.vue")
  },
  // 大屏数据可视化
  {
    path: '/dataScreen',
    name: 'dataScreen',
    component: () => import("@/views/DataScreen/index.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(), //路径显示模式，createWebHashHistory:Hash是#,createWebHistory:History是/
  routes,
});

// 页面路由刚开始切换的时候
router.beforeEach((to, from, next) => {
  // 开启进度条
  NProgress.start();
  // 登录页直接放行
  if (to.path === "/login") return next();
  // 获取token
  const TOKEN = window.sessionStorage.getItem("token");
  // 其他有权限的页面需要判断是否有token，没有就跳转到登录页
  if (!TOKEN) return next("/login");
  // 有token就放行
  next();
});

// 页面路由切换完毕的时候
router.afterEach(() => {
  // 关闭进度条
  NProgress.done();
});

export default router; //导出
