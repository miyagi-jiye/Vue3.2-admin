import axios from "@/axios/axios.js";

// 获取所有权限列表
// - 请求路径：rights/:type
// - 请求方法：get
// - 请求参数
//   type	显示类型 值为 list 或 tree , list：列表显示权限, tree：树状显示权限,参数是url参数:type
export const GET_RIGHTS = (type) => {
  return axios({
    url: `/rights/${type}`,
    method: "get",
  });
};

// 左侧菜单权限
// - 请求路径：menus
// - 请求方法：get
export const GET_MENUS = () => {
  return axios({
    url: "/menus",
    method: "get",
  });
};
