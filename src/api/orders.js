import axios from "@/axios/axios.js";

// 订单数据列表
// - 请求路径：orders
// - 请求方法：get
// - 请求参数
//   参数名                 	参数说明       	备注
//   query               	查询参数       	可以为空
//   pagenum             	当前页码       	不能为空
//   pagesize            	每页显示条数     	不能为空
//   user_id             	用户 ID      	  可以为空
//   pay_status          	支付状态       	可以为空
//   is_send             	是否发货       	可以为空
//   order_fapiao_title  	['个人','公司']	可以为空
//   order_fapiao_company	公司名称       	可以为空
//   order_fapiao_content	发票内容       	可以为空
//   consignee_addr      	发货地址       	可以为空
export const GET_ORDERS = ({ query, pagenum, pagesize, user_id, pay_status, is_send, order_fapiao_title, order_fapiao_company, order_fapiao_content, consignee_addr }) => {
  return axios({
    url: "/orders",
    method: "get",
    params: {
      query,
      pagenum,
      pagesize,
      user_id,
      pay_status,
      is_send,
      order_fapiao_title,
      order_fapiao_company,
      order_fapiao_content,
      consignee_addr,
    },
  });
}

// 修改订单状态
// - 请求路径：orders/:id
// - 请求方法：put
// - 请求参数
//   参数名         	参数说明  	备注                         
//   id          	订单 ID 	不能为空携带在url中                
//   is_send     	订单是否发货	1:已经发货，0:未发货               
//   order_pay   	订单支付  	支付方式 0 未支付 1 支付宝 2 微信 3 银行卡
//   order_price 	订单价格  	                           
//   order_number	订单数量  	                           
//   pay_status  	支付状态  	订单状态： 0 未付款、1 已付款          
// - 请求数据说明
//   - 所有请求数据都是增量更新，如果参数不填写，就不会更新该字段
export const UPDATE_ORDERS = ({ order_id, is_send, order_pay, order_price, order_number, pay_status }) => {
  return axios({
    url: `/orders/${order_id}`,
    method: "put",
    data: {
      is_send,
      order_pay,
      order_price,
      order_number,
      pay_status,
    },
  });
}

// 查看订单详情
// - 请求路径：orders/:id
// - 请求方法：get
// - 请求参数
//   参数名 	参数说明 	备注         
//   id  	订单 ID	不能为空携带在url中
export const GET_ORDERS_DETAIL = (id) => {
  return axios({
    url: `/orders/${id}`,
    method: "get",
  });
}

// 查看物流信息(已失效，mockjs替代)
// - 请求路径：/kuaidi/:id
// - 请求方法：get
// - 供测试的物流单号：1106975712662
export const GET_KUAI_DI = (id) => {
  return axios({
    url: `/kuaidi/${id}`,
    method: "get",
  });
}


