import axios from "@/axios/axios.js";

// 获取菜单
// - 请求路径：menus
// - 请求方法：get
export const GET_MENUS = () => {
  return axios({
    url: "/menus",
    method: "get",
  });
};
