import axios from "@/axios/axios.js";

// 登录
export const LOGIN = ({ username, password }) => {
  return axios({
    url: "/login",
    method: "post",
    params: { username, password },
  });
};
