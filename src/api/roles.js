import axios from "@/axios/axios.js";

// 获取角色列表
// - 请求路径：roles
// - 请求方法：get
export const GET_ROLES = () => {
  return axios({
    url: "/roles",
    method: "get",
  });
};

// 获取所有权限列表
// - 请求路径：rights/:type
// - 请求方法：get
// - 请求参数
//   type	显示类型 值为 list 或 tree , list：列表显示权限, tree：树状显示权限,参数是url参数:type
export const GET_RIGHTS = (type) => {
  return axios({
    url: `/rights/${type}`,
    method: "get",
  });
};

// 添加角色
// - 请求路径：roles
// - 请求方法：post
// - 请求参数
//   roleName	角色名称	不能为空
//   roleDesc	角色描述	可以为空
export const ADD_ROLES = ({ roleName, roleDesc }) => {
  return axios({
    url: "/roles",
    method: "post",
    data: { roleName, roleDesc },
  });
};

// 根据 ID 查询角色
// - 请求路径：roles/:id
// - 请求方法：get
// - 请求参数
//   :id 角色ID 不能为空携带在url中
export const GET_ROLES_BY_ID = (id) => {
  return axios({
    url: `/roles/${id}`,
    method: "get",
  });
};

// 编辑提交角色
// - 请求路径：roles/:id
// - 请求方法：put
// - 请求参数
//   :id     	角色ID	不能为空携带在url中
//   roleName	角色名称 	不能为空
//   roleDesc	角色描述 	可以为空
export const EDIT_ROLES = ({ id, roleName, roleDesc }) => {
  return axios({
    url: `/roles/${id}`,
    method: "put",
    data: { roleName, roleDesc },
  });
};

// 删除角色
// - 请求路径：roles/:id
// - 请求方法：delete
// - 请求参数
//   :id 角色ID 不能为空携带在url中
export const DELETE_ROLES = (id) => {
  return axios({
    url: `/roles/${id}`,
    method: "delete",
  });
};

// 角色授权
// - 请求路径：roles/:id/rights
// - 请求方法：post
// - 请求参数：通过 请求体 发送给后端
//   :id	角色ID	不能为空携带在url中
//   rids 权限ID列表（字符串） 以 , 分割的权限ID列表（获取所有被选中、叶子节点的key和半选中节点的key, 包括 1，2，3级节点）
export const ROLES_AUTHORIZATION = ({ id }, rids) => {
  return axios({
    url: `/roles/${id}/rights`,
    method: "post",
    data: { rids },
  });
};

// 删除角色指定权限
// - 请求路径：roles/:id/rights/:rightId
// - 请求方法：delete
// - 请求参数
//   :id 	角色ID	不能为空携带在url中
//   :rightId	权限ID	不能为空携带在url中
export const DELETE_ROLES_RIGHTS = ({ id, rightId }) => {
  return axios({
    url: `/roles/${id}/rights/${rightId}`,
    method: "delete",
  });
};
