import axios from "axios";

// 商品分类数据列表
// - 请求路径：categories
// - 请求方法：get
// - 请求参数
//   参数名     	参数说明     	备注                                      
//   type    	[1,2,3]  	值：1，2，3 分别表示显示一层二层三层分类列表<br />【可选参数】如果不传递，则默认获取所有级别的分类
//   pagenum 	当前页码值    	【可选参数】如果不传递，则默认获取所有分类                   
//   pagesize	每页显示多少条数据	【可选参数】如果不传递，则默认获取所有分类      
export const GET_CATEGORIES = ({ type, pagenum, pagesize }) => {
  return axios({
    url: "/categories",
    method: "get",
    params: { type, pagenum, pagesize },
  });
}

// 添加分类
// - 请求路径：categories
// - 请求方法：post
// - 请求参数
//   参数名      	参数说明  	备注                           
//   cat_pid  	分类父 ID	不能为空，如果要添加1级分类，则父分类Id应该设置为  0
//   cat_name 	分类名称  	不能为空                         
//   cat_level	分类层级  	不能为空，0表示一级分类；1表示二级分类；2表示三级分类 
export const ADD_CATEGORY = ({ cat_pid, cat_name, cat_level }) => {
  return axios({
    url: "/categories",
    method: "post",
    data: { cat_pid, cat_name, cat_level },
  });
}

// 根据 id 查询分类
// - 请求路径：categories/:id
// - 请求方法：get
// - 请求参数
//   参数名 	参数说明 	备注         
//   :id 	分类 ID	不能为空携带在url中
export const GET_CATEGORY = ({cat_id}) => {
  return axios({
    url: `/categories/${cat_id}`,
    method: "get",
  });
}

// 编辑提交分类
// - 请求路径：categories/:id
// - 请求方法：put
// - 请求参数
//   参数名     	参数说明 	备注              
//   :id     	分类 ID	不能为空携带在url中     
//   cat_name	分类名称 	不能为空【此参数，放到请求体中】
export const EDIT_CATEGORY = ({ cat_id, cat_name }) => {
  return axios({
    url: `/categories/${cat_id}`,
    method: "put",
    data: { cat_name },
  });
}

// 删除分类
// - 请求路径：categories/:id
// - 请求方法：delete
// - 请求参数
//   参数名 	参数说明 	备注         
//   :id 	分类 ID	不能为空携带在url中
export const DELETE_CATEGORY = ({ cat_id }) => {
  return axios({
    url: `/categories/${cat_id}`,
    method: "delete",
  });
}




