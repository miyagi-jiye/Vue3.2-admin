import axios from "axios";

// 参数列表
// - 请求路径：categories/:id/attributes
// - 请求方法：get
// - 请求参数
//   参数名 	参数说明       	备注                                 
//   :id 	分类 ID      	不能为空携带在url中                        
//   sel 	[only,many]	不能为空,通过 only 或 many 来获取分类静态参数还是动态参数
export const GET_ATTRIBUTES = (id, sel) => {
  return axios({
    url: `/categories/${id}/attributes`,
    method: "get",
    params: { sel },
  });
}

// 添加动态参数或者静态属性
// - 请求路径：categories/:id/attributes
// - 请求方法：post
// - 请求参数
//   参数名      	参数说明                    	备注         
//   :id      	分类 ID                   	不能为空携带在url中
//   attr_name	参数名称                    	不能为空       
//   attr_sel 	[only,many]             	不能为空       
//   attr_vals	如果是 many 就需要填写值的选项，以逗号分隔	【可选参数】 
export const ADD_ATTRIBUTE = (id, {attr_name, attr_sel, attr_vals}) => {
  return axios({
    url: `/categories/${id}/attributes`,
    method: "post",
    data: { attr_name, attr_sel, attr_vals },
  });
}


// 删除参数
// - 请求路径： categories/:id/attributes/:attrid
// - 请求方法：delete
// - 请求参数
//   参数名    	参数说明 	备注         
//   :id    	分类 ID	不能为空携带在url中
//   :attr_id	参数 ID	不能为空携带在url中
export const DELETE_ATTRIBUTE = (id, attr_id) => {
  return axios({
    url: `/categories/${id}/attributes/${attr_id}`,
    method: "delete",
  });
}

// 根据 ID 查询参数
// - 请求路径：categories/:id/attributes/:attrId
// - 请求方法：get
// - 请求参数
//   参数名      	参数说明                    	备注         
//   :id      	分类 ID                   	不能为空携带在url中
//   :attrId  	属性 ID                   	不能为空携带在url中
//   attr_sel 	[only,many]             	不能为空       
//   attr_vals	如果是 many 就需要填写值的选项，以逗号分隔	     
export const GET_ATTRIBUTE = (id, attrId, attr_sel, attr_vals) => {
  return axios({
    url: `/categories/${id}/attributes/${attrId}`,
    method: "get",
    params: { attr_sel, attr_vals },
  });
}

// 编辑提交参数
// - 请求路径：categories/:id/attributes/:attrId
// - 请求方法：put
// - 请求参数
//   参数名      	参数说明            	备注          
//   :id      	分类 ID           	不能为空携带在url中 
//   :attrId  	属性 ID           	不能为空携带在url中 
//   attr_name	新属性的名字          	不能为空，携带在请求体中
//   attr_sel 	属性的类型[many或only]	不能为空，携带在请求体中
//   attr_vals	参数的属性值          	可选参数，携带在请求体中
export const EDIT_ATTRIBUTE = (id, {attrId, attr_name, attr_sel, attr_vals}) => {
  return axios({
    url: `/categories/${id}/attributes/${attrId}`,
    method: "put",
    data: { attr_name, attr_sel, attr_vals },
  });
}

// 商品分类数据列表
// - 请求路径：categories
// - 请求方法：get
// - 请求参数
//   参数名     	参数说明     	备注                                      
//   type    	[1,2,3]  	值：1，2，3 分别表示显示一层二层三层分类列表<br />【可选参数】如果不传递，则默认获取所有级别的分类
//   pagenum 	当前页码值    	【可选参数】如果不传递，则默认获取所有分类                   
//   pagesize	每页显示多少条数据	【可选参数】如果不传递，则默认获取所有分类      
export const GET_CATEGORIES = () => {
  return axios({
    url: "/categories",
    method: "get",
  });
}


