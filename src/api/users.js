import axios from "@/axios/axios.js";

// 获取用户数据列表
// - 请求路径：users
// - 请求方法：get
export const GET_USERS = ({ query, pagenum, pagesize }) => {
  return axios({
    url: "/users",
    method: "get",
    params: { query, pagenum, pagesize },
  });
};

// 添加用户
// - 请求路径：users
// - 请求方法：post
export const ADD_USERS = ({ username, password, email, mobile }) => {
  return axios({
    url: "/users",
    method: "post",
    data: { username, password, email, mobile }, //注意：data参数和params参数不能同时使用
  });
};

// 修改用户状态
// - 请求路径：users/:uId/state/:type
// - 请求方法：put
export const CHANGE_USERS_STATUS = (id, status) => {
  return axios({
    url: `/users/${id}/state/${status}`,
    method: "put",
  });
};

// 根据 ID 查询用户信息
// - 请求路径：users/:id
// - 请求方法：get
export const GET_USERS_BY_ID = (id) => {
  return axios({
    url: `/users/${id}`,
    method: "get",
  });
};

// 编辑用户信息并提交
// - 请求路径：users/:id
// - 请求方法：put
export const EDIT_USERS = ({ id, email, mobile }) => {
  return axios({
    url: `/users/${id}`,
    method: "put",
    data: { email, mobile }, //注意：data参数和params参数的区别是：params参数是get请求的参数，data参数是post请求的参数
  });
};

// 删除单个用户
// - 请求路径：users/:id
// - 请求方法：delete
export const DELETE_USERS = (id) => {
  return axios({
    url: `/users/${id}`,
    method: "delete",
  });
};

// 分配用户角色
// - 请求路径：users/:id/role
// - 请求方法：put
export const ASSIGN_USERS_ROLE = (id, rid) => {
  return axios({
    url: `/users/${id}/role`,
    method: "put",
    data: { rid }, //params是role?rid形式的
  });
};

// 获取所有角色列表
// - 请求路径：roles
// - 请求方法：get
export const GET_ROLES = () => {
  return axios({
    url: "/roles",
    method: "get",
  });
};
