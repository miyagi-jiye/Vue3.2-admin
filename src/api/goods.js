import axios from "@/axios/axios.js";

// 商品分类数据列表
// - 请求路径：categories
// - 请求方法：get
// - 请求参数
//   参数名     	参数说明     	备注                                      
//   type    	[1,2,3]  	值：1，2，3 分别表示显示一层二层三层分类列表<br />【可选参数】如果不传递，则默认获取所有级别的分类
//   pagenum 	当前页码值    	【可选参数】如果不传递，则默认获取所有分类                   
//   pagesize	每页显示多少条数据	【可选参数】如果不传递，则默认获取所有分类      
export const GET_CATEGORIES = () => {
  return axios({
    url: "/categories",
    method: "get",
  });
}

// 参数列表
// - 请求路径：categories/:id/attributes
// - 请求方法：get
// - 请求参数
//   参数名 	参数说明       	备注                                 
//   :id 	分类 ID      	不能为空携带在url中                        
//   sel 	[only,many]	不能为空,通过 only 或 many 来获取分类静态参数还是动态参数
export const GET_ATTRIBUTES = ({ id, sel }) => {
  return axios({
    url: `/categories/${id}/attributes`,
    method: "get",
    params: { sel },
  });
}

// 商品列表数据
// - 请求路径：goods
// - 请求方法：get
// - 请求参数
//   参数名     	参数说明  	备注
//   query   	查询参数  	可以为空
//   pagenum 	当前页码  	不能为空
//   pagesize	每页显示条数	不能为空
export const GET_GOODS = ({ query, pagenum, pagesize }) => {
  return axios({
    url: "/goods",
    method: "get",
    params: { query, pagenum, pagesize },
  });
};

// 添加商品
// - 请求路径：goods
// - 请求方法：post
// - 请求参数
//   参数名            	参数说明                    	备注
//   goods_name     	商品名称                    	不能为空
//   goods_cat      	以为','分割的分类列表            	不能为空
//   goods_price    	价格                      	不能为空
//   goods_number   	数量                      	不能为空
//   goods_weight   	重量                      	不能为空
//   goods_introduce	介绍                      	可以为空
//   pics           	上传的图片临时路径（对象）           	可以为空
//   attrs          	商品的参数（数组），包含动态参数和静态属性	可以为空
export const ADD_GOODS = ({
  goods_name,
  goods_cat,
  goods_price,
  goods_number,
  goods_weight,
  goods_introduce,
  pics,
  attrs,
}) => {
  return axios({
    url: "/goods",
    method: "post",
    data: {
      goods_name,
      goods_cat: goods_cat.join(","),
      goods_price,
      goods_number,
      goods_weight,
      goods_introduce,
      pics,
      attrs,
    },
  });
};

// 根据 ID 查询商品
// - 请求路径：goods/:id
// - 请求方法：get
// - 请求参数
//   参数名 	参数说明 	备注
//   id  	商品 ID	不能为空携带在url中
export const GET_GOODS_BY_ID = (id) => {
  return axios({
    url: `/goods/${id}`,
    method: "get",
  });
};

// 编辑提交商品
// - 请求路径：goods/:id
// - 请求方法：put
// - 请求参数
//   参数名            	参数说明         	备注
//   id             	商品 ID        	不能为空携带在url中
//   goods_name     	商品名称         	不能为空
//   goods_cat      	以为','分割的分类列表            	不能为空
//   goods_price    	价格           	不能为空
//   goods_number   	数量           	不能为空
//   goods_weight   	重量           	不能为空
//   goods_introduce	介绍           	可以为空
//   pics           	上传的图片临时路径（对象）	可以为空
//   attrs          	商品的参数（数组）    	可以为空
export const EDIT_GOODS = ({
  id,
  goods_name,
  goods_cat,
  goods_price,
  goods_number,
  goods_weight,
  goods_introduce,
  pics,
  attrs,
}) => {
  return axios({
    url: `/goods/${id}`,
    method: "put",
    data: {
      goods_name,
      goods_cat,
      goods_price,
      goods_number,
      goods_weight,
      goods_introduce,
      pics,
      attrs,
    },
  });
};

// 删除商品
// - 请求路径：goods/:id
// - 请求方法：delete
// - 请求参数
//   参数名 	参数说明 	备注
//   id  	商品 ID	不能为空携带在url中
export const DELETE_GOODS = (id) => {
  return axios({
    url: `/goods/${id}`,
    method: "delete",
  });
};
