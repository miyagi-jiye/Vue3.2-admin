import axios from 'axios'

// 配置默认请求地址
const server = axios.create({
    baseURL: "http://guowei.fun:8890"
})

// 接口
export const getApiList = () => {
    return server.get('/covid').then(res => ({ ...res.data.data }))
}