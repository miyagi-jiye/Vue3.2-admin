import { defineStore } from 'pinia'
import { getApiList } from '../server/index.js'

export const useStore = defineStore({
  id: 'counter',
  state: () => ({
    list: {},
    item: [],
    chinaAdd: {},
    chinaTotal: {},
    cityDetail: [],
  }),
  actions: {
    async getList() {
      const result = await getApiList()
      // 解构赋值
      const { diseaseh5Shelf, statisGradeCityDetail } = result;
      const { chinaAdd, chinaTotal } = diseaseh5Shelf;
      this.list = result;
      this.chinaAdd = chinaAdd; // 国内新增
      this.chinaTotal = chinaTotal;// 中国疫情总数据
      this.cityDetail = statisGradeCityDetail.slice(0, 20);// 城市详情
      console.log("疫情数据：", result);
      // console.log(this.cityDetail);

      // this.chinaAdd = this.list.diseaseh5Shelf.chinaAdd
      // this.chinaTotal = this.list.diseaseh5Shelf.chinaTotal
      // this.cityDetail = this.list.statisGradeCityDetail.slice(0, 10)
    }
  }
})
