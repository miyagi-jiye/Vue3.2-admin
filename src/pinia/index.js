import usersStore from "@/pinia/module/users.js";
import layoutStore from "@/pinia/module/layout.js";
import rightsStore from "@/pinia/module/rights.js";
import rolesStore from "@/pinia/module/roles.js";
import goodsStore from "@/pinia/module/goods.js";
import loginStore from "@/pinia/module/login.js";
import ordersStore from "@/pinia/module/orders.js";
import categoriesStore from "@/pinia/module/categories.js";
import bangumiStore from "@/pinia/module/bangumi.js";
import paramsStore from "@/pinia/module/params.js";


// 统一导出useStore方法1
// export default function useStore() {
//   // 自定义命名
//   return {
//     users: useUsersStore(),
//     layout: useLayoutStore(),
//   };
// }

// 统一导出useStore方法2
const useStore = () => ({
  loginStore: loginStore(),
  users: usersStore(),
  layout: layoutStore(),
  rights: rightsStore(),
  roles: rolesStore(),
  goods: goodsStore(),
  orders: ordersStore(),
  categories: categoriesStore(),
  bangumi: bangumiStore(),
  params: paramsStore(),
});

export default useStore;
