import { defineStore } from "pinia"
import { LOGIN } from "@/api/login.js"
import { ElMessage, ElNotification } from "element-plus"
import router from "@/router/router.js";

const loginStore = defineStore("login", {
  state: () => ({
    // 登录参数
    loginParams: {
      username: "",
      password: "",
    },
    // 登录获取的数据
    loginData: {}
  }),
  getters: {},
  actions: {
    // 登录
    async REQUEST_login() {
      let res = await LOGIN(this.loginParams)
      console.log("REQUEST_login", res.data);
      if (res.data.meta.status == 200) {
        this.loginData = res.data.data// 登录成功后，保存用户信息
        window.sessionStorage.setItem("token", res.data.data.token);//存储token
        window.sessionStorage.setItem("userInfo", JSON.stringify(res.data.data));//存储用户信息
        router.push("/");//跳转到首页
        ElMessage.success({
          message: '登录成功',
          duratioon: 1000
        })// 成功提示
        return res;
      } else {
        ElMessage.error(res.data.meta.msg); // 错误提示
        return res;
      }
    }
  }
})

export default loginStore
