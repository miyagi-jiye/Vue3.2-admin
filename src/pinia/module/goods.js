import { defineStore } from "pinia"; //定义
import { GET_GOODS, GET_GOODS_BY_ID, ADD_GOODS, EDIT_GOODS, DELETE_GOODS, GET_CATEGORIES, GET_ATTRIBUTES } from "@/api/goods.js"; //获取菜单
import { ElMessage, ElNotification, ElMessageBox } from "element-plus"; //消息提示

const goodsStore = defineStore("goods", {
  state: () => ({
    // 商品列表数据
    goodsList: [],
    // 商品分类数据列表
    categoryList: [],
    // 商品动态参数列表
    manyAttributeList: [],
    // 商品静态属性列表
    onlyAttributeList: [],
    // 图片上传地址
    uploadUrl: "http://124.223.49.16:8889/api/private/v1/upload",
    // 商品列表总数
    total: 0,
    // 过滤关键字
    filterKeyword: "",
    // 搜索id
    searchId: "",
    // 编辑商品弹框显示状态
    editDialog: false,
    // 商品列表请求参数
    goodsQuery: {
      query: "",
      pagenum: 1,
      pagesize: 10,
    },
    // 添加商品请求参数
    addGoodsParams: {
      goods_name: "测试商品",
      goods_cat: [],
      goods_price: "1",
      goods_number: "1",
      goods_weight: "1",
      goods_introduce: "测试",//可以为空
      pics: [],//可以为空
      attrs: [],//可以为空
    },
    // 编辑商品请求参数
    editGoodsParams: {
      id: "",
      goods_name: "",
      goods_cat: "1,2,3",
      goods_price: 0,
      goods_number: 0,
      goods_weight: 0,
      goods_introduce: "",//可以为空
      pics: [],//可以为空
      attrs: [],//可以为空
    },
    // 删除商品请求参数
    deleteGoodsParams: {
      id: "",
    },
    // 商品动态参数请求参数
    attributeParams: {
      id: '',
      sel: "many"
    },
    // 商品静态属性请求参数
    attributeParams1: {
      id: '',
      sel: "only"
    },
  }),
  getters: {},
  actions: {
    // 获取商品列表数据
    async REQUEST_goodsList() {
      let res = await GET_GOODS(this.goodsQuery);
      console.log("REQUEST_goodsList", res.data);
      if (res.data.meta.status == 200) {
        this.goodsList = res.data.data.goods;
        this.total = res.data.data.total;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 根据 ID 查询商品
    async REQUEST_goodsById() {
      let res = await GET_GOODS_BY_ID(this.searchId);
      console.log("REQUEST_goodsById", res.data);
      if (res.data.meta.status == 200) {
        this.goodsList = [res.data.data];//返回值是对象要转为数组
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 添加商品
    async REQUEST_addGoods() {
      let res = await ADD_GOODS(this.addGoodsParams);
      console.log("REQUEST_addGoods", res.data);
      if (res.data.meta.status == 201) {
        ElMessage.success(res.data.meta.msg); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 编辑商品
    async REQUEST_editGoods() {
      let res = await EDIT_GOODS(this.editGoodsParams);
      console.log("REQUEST_editGoods", res.data);
      if (res.data.meta.status == 200) {
        this.REQUEST_goodsList();// 成功后重新请求列表数据
        ElMessage.success('修改成功'); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 删除商品
    REQUEST_deleteGoods() {
      ElMessageBox.confirm("此操作将永久删除该角色, 是否继续?", "提示", {
        confirmButtonText: "确认",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(async () => {
          let res = await DELETE_GOODS(this.deleteGoodsParams.id);
          console.log("REQUEST_deleteGoods", res.data);
          if (res.data.meta.status == 200) {
            this.REQUEST_goodsList();// 删除成功后重新请求列表数据
            ElMessage({ type: "success", message: "删除成功." }); // 成功提示
            return res;
          } else {
            ElMessage({ type: "error", message: res.data.meta.msg }); // 错误提示
            return res;
          }
        })
        .catch(() => {
          ElMessage({ type: "info", message: "已取消删除." });
        });

    },
    // 获取商品分类数据列表
    async REQUEST_categoryList() {
      let res = await GET_CATEGORIES();
      console.log("REQUEST_categoryList", res.data);
      if (res.data.meta.status == 200) {
        this.categoryList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取商品静态属性列表
    async REQUEST_onlyAttributeList() {
      // 静态属性
      let res = await GET_ATTRIBUTES(this.attributeParams1);
      console.log("REQUEST_onlyAttributeList", res.data);
      if (res.data.meta.status == 200) {
        this.onlyAttributeList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
      }
    },
    // 获取商品动态参数列表
    async REQUEST_manyAttributeList() {
      // 动态参数
      let res = await GET_ATTRIBUTES(this.attributeParams);
      console.log("REQUEST_manyAttributeList", res.data);
      if (res.data.meta.status == 200) {
        // 截取字符串将attr_vals中以空格为分隔的数据转换为数组
        res.data.data.forEach((item) => {
          item.attr_vals = item.attr_vals.length === 0 ? [] : item.attr_vals.split(',')
        })
        this.manyAttributeList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
      }
    }
  }
});

export default goodsStore;
