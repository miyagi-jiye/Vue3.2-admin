import { defineStore } from "pinia"; //定义
import { GET_ORDERS, UPDATE_ORDERS, GET_ORDERS_DETAIL, GET_KUAI_DI } from "@/api/orders.js"; //引入接口
import { ElMessage, ElNotification } from "element-plus"; //消息提示

const ordersStore = defineStore("orders", {
  state: () => ({
    // 订单列表
    ordersList: [],
    // 订单列表请求参数
    ordersListParams: {
      pagenum: 1,
      pagesize: 10,
    },
    // 订单总数
    total: 0,
    // 快递信息
    kuaiDi: {},
    // 快递dialog框是否显示
    kuaiDiDialog: false,
    // 订单详情数据
    orderDetail: {},
    // 修改订单dialog框是否显示
    editDialog: false,
  }),
  getters: {},
  actions: {
    // 获取订单列表
    async REQUEST_ordersList() {
      let res = await GET_ORDERS(this.ordersListParams);
      console.log("REQUEST_ordersList", res.data);
      if (res.data.meta.status == 200) {
        this.ordersList = res.data.data.goods; //添加到订单列表
        this.total = res.data.data.total; //添加到订单总数
        // ElNotification.success(res.data.meta.msg); // 成功提示，一般不显示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取物流信息
    async REQUEST_kuaiDi(id) {
      let res = await GET_KUAI_DI(id);
      console.log("REQUEST_kuaiDi", res.data);
      if (res.data.meta.status == 200) {
        this.kuaiDi = res.data.data; //添加到快递信息
        // ElNotification.success(res.data.meta.msg); // 成功提示，一般不显示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取订单详情
    async REQUEST_ordersDetail(id) {
      let res = await GET_ORDERS_DETAIL(id);
      console.log("REQUEST_ordersDetail", res.data);
      if (res.data.meta.status == 200) {
        this.orderDetail = res.data.data; //添加到订单详情
        // ElNotification.success(res.data.meta.msg); // 成功提示，一般不显示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 修改订单信息
    async REQUEST_editOrder() {
      let res = await UPDATE_ORDERS(this.orderDetail);
      console.log("REQUEST_editOrder", res.data);
      if (res.data.meta.status == 201) {
        ElNotification.success(res.data.meta.msg); // 成功提示
        this.editDialog = false; //关闭dialog框
        this.REQUEST_ordersList(); //重新请求订单列表
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    }
  },
});

export default ordersStore;