// 首先我们需要在 actions 里面定义一个方法
import { defineStore } from "pinia";

export const useStore = defineStore("main", {
  state: () => ({
    count: 10,
    num: 20,
  }),
  getters: {
    getCount(state) {
      return state.count * 2;
    }
  },
  actions: {
    piniaAdd() {
      this.count++;
      /* 
      特别注意：在这里this指向的就是当前的实例化出来的对象，
      piniaAdd 该函数如果换成箭头函数的话，
      this 指向就会发生 改变，不能再使用 this.count++; 了
      **/
    },
  },
});
