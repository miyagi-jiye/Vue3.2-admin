import { defineStore } from "pinia"; //定义
import {
  GET_ROLES,
  ADD_ROLES,
  GET_ROLES_BY_ID,
  EDIT_ROLES,
  DELETE_ROLES,
  ROLES_AUTHORIZATION,
  DELETE_ROLES_RIGHTS,
  GET_RIGHTS,
} from "@/api/roles.js"; //引入接口
import { ElMessage, ElNotification, ElMessageBox } from "element-plus"; //消息提示

const rolesStore = defineStore("roles", {
  state: () => ({
    // 角色列表数据
    rolesList: [],
    // 权限列表数据
    rightsList: [],
    // 树形列表默认选中的权限的id
    defKeys: [],
    // 权限列表显示类型list/tree
    type: "tree",
    // 添加角色弹出框
    addDialog: false,
    // 编辑角色弹出框
    editDialog: false,
    // 分配角色权限弹出框
    assignDialog: false,
    // 添加角色请求参数
    addForm: {
      roleName: "",
      roleDesc: "",
    },
    // 根据ID删除指定权限请求参数
    deleteParams: {
      id: "",
      rightId: "",
    },
    // 当前要删除的角色的信息
    deleteRole: {},
    // 当前要编辑的角色的信息
    editRole: {},
    // 当前要分配权限的角色的信息
    assignRights: {},
    // 当前选中的和半选中的权限id字符串
    rids: "",
    // 搜索ID
    searchId: "",
    // 搜索返回值
    searchList: {},
  }),
  getters: {
    // TODO:根据searchId过滤角色列表数据
    rolesListFilter: (state) =>
      state.rolesList.filter(
        (item) => !state.searchId || item.id.toString().includes(state.searchId)
      ),
  },
  actions: {
    // 获取角色列表
    async REQUEST_rolesList() {
      let res = await GET_ROLES();
      console.log("REQUEST_rolesList", res.data);
      if (res.data.meta.status == 200) {
        this.rolesList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 添加角色
    async REQUEST_addRoles() {
      let res = await ADD_ROLES(this.addForm);
      console.log("REQUEST_addRoles", res.data);
      if (res.data.meta.status == 201) {
        ElNotification.success("添加成功"); // 成功提示
        this.addDialog = false; // 关闭弹出框
        this.REQUEST_rolesList(); // 刷新列表
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 删除角色
    REQUEST_deleteRoles() {
      // confirm(message, title, options)
      ElMessageBox.confirm("此操作将永久删除该角色, 是否继续?", "提示", {
        confirmButtonText: "确认",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(async () => {
          let res = await DELETE_ROLES(this.deleteRole.id);
          console.log("REQUEST_deleteRoles", res.data);
          if (res.data.meta.status == 200) {
            ElMessage({
              type: "success",
              message: "删除成功",
            }); // 成功提示
            this.REQUEST_rolesList(); // 刷新列表
            return res;
          } else {
            ElNotification.error(res.data.meta.msg); // 错误提示
            return res;
          }
        })
        .catch(() => {
          ElMessage({
            type: "info",
            message: "已取消删除.",
          });
        });
    },
    // 编辑角色
    async REQUEST_editRoles() {
      let res = await EDIT_ROLES(this.editRole);
      console.log("REQUEST_editRoles", res.data);
      if (res.data.meta.status == 200) {
        ElNotification.success("修改成功"); // 成功提示
        this.editDialog = false; // 关闭弹出框
        this.REQUEST_rolesList(); // 刷新列表
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取权限列表
    async REQUEST_rightsList() {
      let res = await GET_RIGHTS(this.type);
      console.log("REQUEST_rightsList", res.data);
      if (res.data.meta.status == 200) {
        this.rightsList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 分配角色权限
    async REQUEST_assignRights() {
      let res = await ROLES_AUTHORIZATION(this.assignRights, this.rids);
      console.log("REQUEST_assignRights", res.data);
      if (res.data.meta.status == 200) {
        ElNotification.success("分配成功"); // 成功提示
        this.assignDialog = false; // 关闭弹出框
        this.REQUEST_rolesList(); // 刷新列表
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 根据 ID 删除角色指定权限
    REQUEST_deleteRights() {
      ElMessageBox.confirm("此操作将永久删除该角色, 是否继续?", "提示", {
        confirmButtonText: "确认",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(async () => {
          let res = await DELETE_ROLES_RIGHTS(this.deleteParams);
          console.log("REQUEST_deleteRights", res.data);
          if (res.data.meta.status == 200) {
            // TODO:未解决重新渲染问题
            // 防止列表刷新，提升用户体验
            this.rolesList.forEach((item) => {
              if (item.id == this.deleteParams.id) {
                item.children = res.data.data;
                // console.log("修改前", item.children, "修改后", res.data.data);
              }
            });
            ElMessage({
              type: "success",
              message: "删除成功",
            }); // 成功提示
            return res;
          } else {
            ElNotification.error(res.data.meta.msg); // 错误提示
            return res;
          }
        })
        .catch(() => {
          ElMessage({
            type: "info",
            message: "已取消删除.",
          });
        });
    },
    // TODO:根据 ID 查询角色
    async REQUEST_getRolesById() {
      let res = await GET_ROLES_BY_ID(Number(this.searchId));
      console.log("REQUEST_getRolesById", res.data);
      if (res.data.meta.status == 200) {
        this.searchList = res.data.data; // 查询结果
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
  },
});

export default rolesStore;
