import { defineStore } from "pinia"; //定义
import {
  GET_USERS,
  CHANGE_USERS_STATUS,
  DELETE_USERS,
  ADD_USERS,
  EDIT_USERS,
  ASSIGN_USERS_ROLE,
  GET_ROLES,
} from "@/api/users"; //引入接口
import { ElMessage, ElNotification } from "element-plus"; //消息提示

const usersStore = defineStore("users", {
  state: () => ({
    // 分页数据
    queryInfo: {
      query: "",
      pagenum: 1, //当前页
      pagesize: 10, //每页显示条数
    },
    // 添加用户的表单数据
    addInfo: {
      username: "测试用户",
      password: "123456",
      email: "123456789@qq.com",
      mobile: "15736569178",
    },
    // 用户数据列表
    usersList: [],
    // 用户数据总数
    total: 0,
    // 当前编辑的用户数据
    editUsers: {},
    // 当前编辑用户框的显示
    editDialogVisible: false,
    // 当前添加用户框的显示
    addDialogVisible: false,
    // 当前分配角色的抽屉的显示
    editDrawer: false,
    // 当前分配角色的抽屉的弹出位置
    editDirection: "ltr",
    // 当前分配角色的数据,里面有id
    editRole: {},
    // 所有可选角色数据列表,rid就是这里面的id
    rolesList: [],
    // 当前分配角色下拉列表选中的rid
    selectId: "",
  }),
  getters: {
    // 过滤用户数据列表
    GET_usersListFilter() {
      return this.usersList.filter();
    },
  },
  actions: {
    // 获取用户数据列表
    async REQUEST_usersList() {
      let res = await GET_USERS(this.queryInfo);
      console.log("REQUEST_usersList", res.data);
      if (res.data.meta.status == 200) {
        this.usersList = res.data.data.users; //添加到用户数据列表
        this.total = res.data.data.total; //添加到用户数据总数
        // ElNotification.success(res.data.meta.msg); // 成功提示，一般不显示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 更改用户状态
    async REQUEST_usersStatus(id, status) {
      let res = await CHANGE_USERS_STATUS(id, status);
      console.log("REQUEST_usersStatus", res.data);
      if (res.data.meta.status == 200) {
        ElNotification.success(res.data.meta.msg); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 删除用户
    async REQUEST_usersDelete(id) {
      let res = await DELETE_USERS(id);
      console.log("REQUEST_usersDelete", res.data);
      if (res.data.meta.status == 200) {
        ElNotification.success(res.data.meta.msg); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 添加用户
    async REQUEST_usersAdd() {
      let res = await ADD_USERS(this.addInfo);
      /*
      // TODO:前端只能实现数据刷新前的样式，刷新后时间戳会变为原样式，所以不能用这种方法
      // 遍历用户数据列表，取出每一项的id，如果usersList的id和添加用户返回的id相同，就把Creat_time改为当前时间
      this.usersList.forEach((item) => {
        if (item.id == res.data.data.id) {
          item.create_time = new Date().toLocaleString();
        }
      });
      **/
      console.log("REQUEST_usersAdd", res.data);
      if (res.data.meta.status == 201) {
        // this.usersList.push(res.data.data); // 添加到列表去渲染，可以少发送请求，但是缺少用户角色(role_name)
        ElNotification.success(res.data.meta.msg); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 编辑用户
    async REQUEST_usersEdit() {
      let res = await EDIT_USERS(this.editUsers);
      console.log("REQUEST_usersEdit", res.data);
      if (res.data.meta.status == 200) {
        ElNotification.success(res.data.meta.msg); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取所有角色列表
    async REQUEST_rolesList() {
      let res = await GET_ROLES();
      console.log("REQUEST_rolesList", res.data);
      if (res.data.meta.status == 200) {
        this.rolesList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 分配用户角色
    async REQUEST_usersRole() {
      let res = await ASSIGN_USERS_ROLE(this.editRole.id, this.selectId);
      console.log("REQUEST_usersRole", res.data);
      if (res.data.meta.status == 200) {
        ElNotification.success(res.data.meta.msg); // 成功提示
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
  },
});

export default usersStore;
