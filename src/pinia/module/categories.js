import { defineStore } from "pinia"; //定义
import { GET_CATEGORIES, ADD_CATEGORY, GET_CATEGORY, EDIT_CATEGORY, DELETE_CATEGORY } from "@/api/categories.js"; //api接口
import { ElMessage, ElNotification, ElMessageBox } from "element-plus"; //消息提示

const categoriesStore = defineStore("categories", {
  state: () => ({
    // 分类列表数据
    categoriesList: [],
    // 所有分类列表数据
    categoriesListALL: [],
    // 分类列表请求参数
    categoriesQuery: {
      type: "",//分类层级，不填写默认获取所有
      pagenum: 1,//页码
      pagesize: 10//每页条数
    },
    // 分类数据总数
    total: 0,
    // 编辑框显示
    editDialog: false,
    // 编辑分类参数
    editCategoriesQuery: {
      cat_id: "",
      cat_name: '',
    },
    // 删除分类参数
    deleteCategoriesQuery: {
      cat_id: "",
    },
    // 添加框显示
    addDialog: false,
    // 添加分类参数
    addCategoriesQuery: {
      cat_pid: "0",
      cat_name: '',
      cat_level: "0",
    },
    // 根据id查询分类参数
    searchCategoriesQuery: {
      cat_id: '',
    },
  }),
  getters: {},
  actions: {
    // 获取分类列表数据
    async REQUEST_categoriesList() {
      let res = await GET_CATEGORIES(this.categoriesQuery);
      console.log("REQUEST_categoriesList", res.data);
      if (res.data.meta.status == 200) {
        this.categoriesList = res.data.data.result;
        this.total = res.data.data.total;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取所有分类列表数据
    async REQUEST_categoriesListAll() {
      let res = await GET_CATEGORIES({ type: this.addCategoriesQuery.cat_level, pagenum: '', pagesize: '' });
      console.log("REQUEST_categoriesListAll", res.data);
      if (res.data.meta.status == 200) {
        this.categoriesListALL = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 编辑分类名称
    async REQUEST_editCategories() {
      let res = await EDIT_CATEGORY(this.editCategoriesQuery);
      console.log("REQUEST_editCategories", res.data);
      if (res.data.meta.status == 200) {
        ElMessage.success("修改成功"); // 成功提示
        this.editDialog = false;//关闭编辑框
        this.REQUEST_categoriesList();//刷新列表
        return res;
      } else {
        ElMessage.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 删除分类
    REQUEST_deleteCategories() {
      ElMessageBox.confirm("此操作将永久删除该角色, 是否继续?", "提示", {
        confirmButtonText: "确认",
        cancelButtonText: "取消",
        type: "warning",
      }).then(async () => {
        let res = await DELETE_CATEGORY(this.deleteCategoriesQuery);
        console.log("REQUEST_deleteCategories", res.data);
        if (res.data.meta.status == 200) {
          ElMessage.success("删除成功"); // 成功提示
          this.REQUEST_categoriesList();//刷新列表
          return res;
        } else {
          ElMessage.error(res.data.meta.msg); // 错误提示
          return res;
        }
      }).catch(() => {
        ElMessage({ type: "info", message: "已取消删除." });
      });
    },
    // 添加分类
    // TODO:选择父级分类添加子分类
    async REQUEST_addCategories() {
      let res = await ADD_CATEGORY(this.addCategoriesQuery);
      console.log("REQUEST_addCategories", res.data);
      if (res.data.meta.status == 201) {
        ElMessage.success("添加成功"); // 成功提示
        this.addCategoriesQuery.cat_name = "";//清空添加框内容
        this.REQUEST_categoriesList();//刷新列表
        return res;
      } else {
        ElMessage.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 根据id查询分类
    async REQUEST_searchCategories() {
      let res = await GET_CATEGORY(this.searchCategoriesQuery);
      console.log("REQUEST_searchCategories", res.data);
      if (res.data.meta.status == 200) {
        this.categoriesList = [res.data.data];
        // this.categoriesList.push(res.data.data);
        return res;
      } else {
        ElMessage.error(res.data.meta.msg); // 错误提示
        return res;
      }
    }
  }
})

export default categoriesStore;