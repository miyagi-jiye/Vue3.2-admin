import { defineStore } from "pinia"; //定义
import { GET_MENUS } from "@/api/layout.js"; //获取菜单
import { ElMessage, ElNotification } from "element-plus"; //消息提示

const layoutStore = defineStore("layout", {
  state: () => ({
    menuList: [],
    collapse: false,
    leftDrawer: false,//左侧抽屉
    rightDrawer: false,//右侧抽屉
    tabsHidden: false,//tabs是否隐藏
    logoHidden: false,//logo是否隐藏
    activePath: "/",
    tabsList: [],
    icon: {
      100: "HomeFilled",
      125: "UserFilled",
      103: "HelpFilled",
      101: "Shop",
      102: "List",
      145: "TrendCharts",
      200: "LocationFilled",
    },
  }),
  getters: {
    // 获取Tabs
    GET_tabs() {
      return this.tabsList;
    },
    // 获取菜单
    GET_menuList() {
      return this.menuList;
    },
    // // 获取菜单,箭头函数写法会失去this指向，所以用参数state代替
    // GET_menuList: (state) => state.menusList,
  },
  actions: {
    // 更新折叠状态
    UPDATE_collapse() {
      this.collapse = !this.collapse;
    },
    // 更新激活路径
    UPDATE_activePath(path) {
      this.activePath = path;
    },
    // 判断是否已经存在,如果不存在,才放入
    ADD_tabsList(tab) {
      if (this.tabsList.some((item) => item.path === tab.path)) return;
      this.tabsList.push(tab);
    },
    // 请求菜单，并且更新菜单列表
    REQUEST_menuList() {
      GET_MENUS().then((res) => {
        console.log("REQUEST_menuList", res.data);
        // 错误提示
        if (res.data.meta.status !== 200) ElMessage.error(res.data.meta.msg);
        this.menuList = res.data.data;
        // unshift() 方法将一个或多个元素添加到数组的开头，并返回该数组的新长度。
        this.menuList.unshift(
          {
            id: 100,
            authName: "仪表盘",
            path: "dashboard",
            order: 0,
            children: [
              {
                id: 0,
                authName: "数据可视化",
                path: "dataVisualize",
              },
              {
                id: 1,
                authName: "内嵌页面",
                path: "embedded",
              },
              {
                id: 3,
                authName: "动画时间表(跨域)",
                path: "animation",
                order: 7,
              },
            ],
          }
        );
        // push() 方法将一个或多个元素添加到数组的末尾，并返回该数组的新长度。
        this.menuList.push({
          id: 200,
          authName: "大屏可视化",
          path: "dataScreen",
          order: 6,
        })
      });
    },
  },
});

export default layoutStore;
