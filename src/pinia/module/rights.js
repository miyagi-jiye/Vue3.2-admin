import { defineStore } from "pinia"; //定义
import { GET_RIGHTS, GET_MENUS } from "@/api/rights.js"; //引入接口
import { ElMessage, ElNotification } from "element-plus"; //消息提示

const rightsStore = defineStore("rights", {
  state: () => ({
    // 权限列表数据
    rightsList: [],
    // 权限列表显示类型list/tree
    type: "list",
  }),
  getters: {},
  actions: {
    // 获取权限列表
    async REQUEST_rightsList() {
      let res = await GET_RIGHTS(this.type);
      console.log("REQUEST_rightsList", res.data);
      if (res.data.meta.status == 200) {
        this.rightsList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
  },
});

export default rightsStore;
