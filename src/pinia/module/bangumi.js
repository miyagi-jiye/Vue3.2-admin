import { defineStore } from "pinia"; //定义
import { GET_DAILY_ANIME } from "@/api/bangumi.js"; //api接口
import { ElMessage, ElNotification, ElMessageBox } from "element-plus"; //消息提示

const bangumiStore = defineStore("bangumi", {
  state: () => ({
    // 每日放送
    calendarList: [],
    // 每日放送图片
    calendarImg: [],
  }),
  getters: {},
  actions: {
    // 请求每日放送
    async REQUEST_daily_anime() {
      let res = await GET_DAILY_ANIME();
      console.log("REQUEST_daily_anime", res.data);
      if (res.status == 200) {
        this.calendarList = res.data;//每日放送
        // ElNotification.success("每日放送请求成功"); // 成功提示
        return res;
      } else {
        ElNotification.error("每日放送请求失败"); // 错误提示
        return res;
      }
    }
  }
});

export default bangumiStore;