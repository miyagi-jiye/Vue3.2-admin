import { defineStore } from "pinia"; //定义
import { GET_ATTRIBUTES, ADD_ATTRIBUTE, DELETE_ATTRIBUTE, GET_ATTRIBUTE, EDIT_ATTRIBUTE, GET_CATEGORIES } from "@/api/params.js"; //获取菜单
import { ElMessage, ElNotification, ElMessageBox } from "element-plus"; //消息提示

const paramsStore = defineStore("params", {
  state: () => ({
    // 商品列表数据
    categoryList: [],
    // 动态参数列表
    manyTableData: [],
    // 静态属性列表
    onlyTableData: [],
    // 当前选中的分类id
    currentChecked: '',
    // 按钮禁用状态
    btnDisabled: true,
    // 默认激活的tabs
    activeName: "many",
    // 动态标题
    titleText: "",
    // 新标签
    newTag: "",
    // input显示状态
    inputShow: false,
    // 编辑参数
    editParams: {
      id: '',//当前选中的分类id
      attrId: '',
      attr_name: '',
      attr_sel: '',
      attr_vals: [],//可选参数
    },
    // 添加参数
    addParams: {
      id: '',//当前选中的分类id
      attr_name: '',
      attr_sel: '',
      attr_vals: [],//可选参数
    },
    // 修改对话框
    editDialogVisible: false,
    // 新增对话框
    addDialogVisible: false,
  }),
  getters: {},
  actions: {
    // 获取商品分类数据列表
    async REQUEST_categoryList() {
      let res = await GET_CATEGORIES();
      console.log("REQUEST_categoryList", res.data);
      if (res.data.meta.status == 200) {
        this.categoryList = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 获取商品静态属性列表
    async REQUEST_onlyAttributeList() {
      let res = await GET_ATTRIBUTES(this.currentChecked.at(-1), 'only');
      console.log("REQUEST_onlyAttributeList", res.data);
      if (res.data.meta.status == 200) {

        // 循环分割 attr_vals 的属性值
        res.data.data.forEach((item) => {
          item.attr_vals = item.attr_vals ? item.attr_vals.split(',') : []
          // 控制文本框的显示与隐藏
          item.inputVisible = false
          // 文本框中输入的值
          item.inputValue = ''
        })

        this.onlyTableData = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
      }
    },
    // 获取商品动态参数列表
    async REQUEST_manyAttributeList() {
      let res = await GET_ATTRIBUTES(this.currentChecked.at(-1), 'many');
      console.log("REQUEST_manyAttributeList", res.data);
      if (res.data.meta.status == 200) {

        // 循环分割 attr_vals 的属性值
        res.data.data.forEach((item) => {
          item.attr_vals = item.attr_vals ? item.attr_vals.split(',') : []
          // 控制文本框的显示与隐藏
          item.inputVisible = false
          // 文本框中输入的值
          item.inputValue = ''
        })

        this.manyTableData = res.data.data;
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
      }
    },
    // 删除参数
    REQUEST_deleteParams(attr_id) {
      ElMessageBox.confirm("此操作将永久删除该参数, 是否继续?", "提示", {
        confirmButtonText: "确认",
        cancelButtonText: "取消",
        type: "warning",
      }).then(async () => {
        let res = await DELETE_ATTRIBUTE(this.currentChecked.at(-1), attr_id);
        console.log("REQUEST_deleteParams", res.data);
        if (res.data.meta.status == 200) {
          ElMessage({ type: "success", message: "删除成功" }); // 成功提示
          this.REQUEST_manyAttributeList();
          this.REQUEST_onlyAttributeList(); // 刷新列表
          return res;
        } else {
          ElNotification.error(res.data.meta.msg); // 错误提示
          return res;
        }
      }).catch(() => {
        ElMessage({ type: "info", message: "已取消删除." });
      });
    },
    // 编辑提交参数
    async REQUEST_editParams() {
      let res = await EDIT_ATTRIBUTE(this.currentChecked.at(-1), this.editParams);
      console.log("REQUEST_editParams", res.data);
      if (res.data.meta.status == 200) {
        ElMessage({ type: "success", message: "编辑成功" }); // 成功提示
        this.REQUEST_manyAttributeList();
        this.REQUEST_onlyAttributeList(); // 刷新列表
        this.editDialogVisible = false;// 关闭对话框
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    },
    // 新增提交参数
    async REQUEST_addParams() {
      let res = await ADD_ATTRIBUTE(this.currentChecked.at(-1), this.addParams);
      console.log("REQUEST_addParams", res.data);
      if (res.data.meta.status == 201) {
        ElMessage({ type: "success", message: "新增成功" }); // 成功提示
        this.REQUEST_manyAttributeList();
        this.REQUEST_onlyAttributeList(); // 刷新列表
        this.addDialogVisible = false;// 关闭对话框
        return res;
      } else {
        ElNotification.error(res.data.meta.msg); // 错误提示
        return res;
      }
    }
  }
});

export default paramsStore;