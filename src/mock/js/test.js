import Mock from "mockjs"; // 引入mockjs

const Random = Mock.Random; // Mock.Random 是一个工具类，用于生成各种随机数据

let data = []; // 用于接受生成数据的数组

for (let i = 0; i < 10; i++) {
  // 可自定义生成的个数

  let template = {
    Float: Random.float(0, 100, 0, 5), // 生成0到100之间的浮点数,小数点后尾数为0到5位

    Date: Random.date(), // 生成一个随机日期,可加参数定义日期格式

    Image: Random.image(Random.size, "#02adea", "Hello"), // Random.size表示将从size数据中任选一个数据

    Color: Random.color(), // 生成一个颜色随机值

    Paragraph: Random.paragraph(2, 5), //生成2至5个句子的文本

    Name: Random.name(), // 生成姓名

    Url: Random.url(), // 生成web地址

    Address: Random.province(), // 生成地址
  };

  data.push(template); //每循环一次添加一次，添加模拟数据到数组
}

export default data; //暴露数据
