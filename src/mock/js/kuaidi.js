import Mock from "mockjs"; // 引入mockjs

const Random = Mock.Random; // Mock.Random 是一个工具类，用于生成各种随机数据
// 模拟快递数据
let object = {
  data: [],// 用于接受生成数据的数组
  meta: { status: 200, msg: "获取物流信息成功！" }
};
// 可自定义生成的个数
for (let i = 0; i < 10; i++) {
  // 定义模板
  let template = {
    time: Random.date(), // 生成一个随机日期,可加参数定义日期格式
    ftime: Random.date(), // 生成一个随机日期,可加参数定义日期格式
    context: "快件在[" + Random.province() + "]已装车,准备发往 [" + Random.province() + "]", // 生成地址
    // Paragraph: Random.paragraph(2, 5), //生成2至5个句子的文本
    Name: Random.name(), // 生成姓名
  };
  object.data.push(template); //每循环一次添加一次，添加模拟数据到数组
}

export default object; //暴露数据