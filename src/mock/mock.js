import Mock from "mockjs"; // 引入mockjs
import data1 from "@/mock/js/test.js"; //js 数据写法
import kuaidi from "@/mock/js/kuaidi.js"; //js 数据写法
import data2 from "@/mock/json/test.json"; //json 数据写法
// Mock.mock("/test", "get", data1); // 拦截axios.get("/test")请求，并返回自定义data数据
// Mock.mock("/\/kuaidi*?/", "get", data1); // 拦截axios.get("/kuaidi/:id")请求，并返回自定义kuaidi数据
// Mock.mock('http://124.223.49.16:8889/api/private/v1/test', (options) => { console.debug(options) })


Mock.mock(RegExp("http://124.223.49.16:8889/api/private/v1/kuaidi" + ".*"), "get", kuaidi);// 拦截axios.get("/kuaidi/:id")请求，并返回自定义kuaidi数据

