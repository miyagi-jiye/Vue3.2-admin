import * as XLSX from "xlsx"// 导入xlsx
import { ElNotification } from "element-plus";

/**
 * 导出excel
 * @param {Array} data 导出的数据
 * @param {String} fileName 导出的文件名
 */
export function exportExcel(data, fileName) {
  if (data.length === 0) {
    ElNotification.error("导出的数据为空");
    return;
  }
  ElNotification.warning("正在导出数据，请稍后...");
  const wb = XLSX.utils.book_new();// 创建一个新的工作簿
  const ws = XLSX.utils.json_to_sheet(data);// 将json数据转换成表格数据
  XLSX.utils.book_append_sheet(wb, ws, "Sheet1");// 将表格数据添加到工作簿中
  XLSX.writeFile(wb, fileName ? `${fileName}.xlsx` : '默认文件名.xlsx');// 将工作簿导出为excel文件
}