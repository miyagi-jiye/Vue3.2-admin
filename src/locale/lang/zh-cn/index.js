// 分模块引入
import login from "@/locale/lang/zh-cn/login.js"//登录
import layout from "@/locale/lang/zh-cn/layout.js"//布局

// 统一导出对象
export default {
  ...login,
  ...layout
}