export default {
  login: "登录",
  reset: "重置",
  userPlaceholder: "请输入用户名",
  usernameTips: "用户名长度为3到10位",
  userError: "用户名或密码错误",
  passwordError: "用户名或密码错误",
  passwordTips: "密码长度为6到12位",
  passwordPlaceholder: "请输入密码",
  loginSuccess: "登录成功",
  forgotPassword: "忘记密码",
}