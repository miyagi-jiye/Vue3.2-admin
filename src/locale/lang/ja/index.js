export default {
  login: "ログイン",
  reset: "リセット",
  userError: "ユーザー名またはパスワードエラー",
  usernameTips: "ユーザー名の長さは3から10文字です",
  userPlaceholder: "ユーザー名を入力してください",
  passwordError: "ユーザー名またはパスワードエラー",
  passwordTips: "パスワードの長さは6から12文字です",
  passwordPlaceholder: "パスワードを入力してください",
  loginSuccess: "サインイン成功",
  forgotPassword: "パスワードを忘れた",
}