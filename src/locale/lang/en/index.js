export default {
  login: "Sign in",
  reset: "Reset",
  userError: "Username or password error",
  userPlaceholder: "Please enter username",
  usernameTips: "Username length is 3 to 10",
  passwordError: "Username or password error",
  passwordTips: "Password length is 6 to 12",
  passwordPlaceholder: "Please enter password",
  loginSuccess: "Sign in success",
  forgotPassword: "Forgot password",
}