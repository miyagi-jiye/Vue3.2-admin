import { createI18n } from 'vue-i18n'
// elment-plus国际化设置
import el_zh_CN from "element-plus/es/locale/lang/zh-cn"; //引入element-plus中文语言包
import el_ja_JP from "element-plus/es/locale/lang/ja"; //引入element-plus日文语言包
import el_en_US from "element-plus/es/locale/lang/en"; //引入element-plus英文语言包
// 自定义国际化设置
import zh_CN from "@/locale/lang/zh-cn"; //引入自定义中文语言设置
import ja_JP from "@/locale/lang/ja"; //引入自定义日文语言设置
import en_US from "@/locale/lang/en"; //引入自定义英文语言设置

const messages = {
  'en_US': {
    el: el_en_US,
    ...en_US
  },
  'ja_JP': {
    el: el_ja_JP,
    ...ja_JP
  },
  "zh_CN": {
    el: el_zh_CN,
    ...zh_CN
  }
}

const i18n = createI18n({
  // something vue-i18n options here ...
  legacy: false,//是否使用旧版本的语言包
  locale: sessionStorage.getItem(import.meta.env.VITE_APP_LANG_NAME) || 'zh_CN',//设置默认语言
  fallbackLocale: 'zh_CN',//失败回退语言
  globalInjection: true,//全局注入
  messages,//设置语言包
})

export default i18n