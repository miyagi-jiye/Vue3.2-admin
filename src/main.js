import { createApp } from "vue";
import App from "./App.vue";
import router from "@/router/router.js"; //引入路由
import ElementPlus from "element-plus";//引入element-plus
import "element-plus/dist/index.css"; //引入element-plus样式
import 'element-plus/theme-chalk/dark/css-vars.css'; //引入element-plus暗黑模式主题
import "@/assets/styles/dark/css-vars.css"; //引入自定义暗黑模式主题
import * as ElementPlusIconsVue from "@element-plus/icons-vue"; //element-plus图标
import "@/assets/css/reset.css"; //自定义初始化样式
// import "@/mock/mock.js"; //引入mock拦截请求
import 'animate.css'; //引入动画库
import { createPinia } from "pinia"; // 从pinia中导入createPinia方法
import i18n from "@/locale/index.js"; // 引入i18n
import { useIntersectionObserver } from '@vueuse/core';//引入vueuse中的 是否在视口中 的判断方法

const pinia = createPinia(); // 创建pinia实例
const app = createApp(App);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
} //遍历图标组件并挂载

// console.log(".env文件", import.meta.env);

app.use(i18n);
app.use(router);
app.use(pinia);
app.use(ElementPlus);
// 全局自定义指令,自动聚焦
app.directive("focus", {
  mounted: function (el, binding) {
    el.focus();
    console.log(el, binding);
  }
});
// 全局自定义指令，图片懒加载
app.directive("img-lazy", {
  mounted(el, binding) {
    // 判断el是否在视口中
    const { stop } = useIntersectionObserver(el, ([{ isIntersecting }]) => {
      // isIntersecting 是否在视口中,返回布尔值
      if (isIntersecting) {
        el.src = binding.value;
        stop();
      }
      console.log("图片懒加载触发状态:", isIntersecting, "元素:", el);
    });
  }
});

app.mount("#app");
