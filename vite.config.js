import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";
import { createHtmlPlugin } from 'vite-plugin-html';//引入html插件

// title1
const getViteEnv = (mode, target) => {
  return loadEnv(mode, process.cwd())[target];
};

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '');// title2
  return {
    // 使用的模块
    plugins: [
      vue(),
      // 插件
      createHtmlPlugin({
        inject: {
          data: {
            title: command,// serve or build
            title1: getViteEnv(mode, "VITE_APP_TITLE"),// 动态.env中的指定环境变量
            title2: env.VITE_APP_TITLE,// 动态.env中的指定环境变量
            title3: mode,// development or production
            title4: command == 'serve' ? `${env.VITE_APP_TITLE}(开发模式)` : `${env.VITE_APP_TITLE}(生产模式)`,// 静态.env中的指定环境变量
          },
        },
      }),
    ],
    //本地运行时的路径
    base: "./",
    // 路径配置,path.resolve()拼接文件路径，获取绝对路径，__dirname是当前文件所在的目录
    resolve: {
      // 1.对象形式
      alias: {
        "@": path.resolve(__dirname, "src"),
        'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js',
      },
    },
    // 代理服务器
    server: {
      proxy: {
        "/weather": {
          target: "http://www.weather.com.cn/data/cityinfo/101040100.html", // 将/api访问转换成target
          changeOrigin: true, // 允许跨域
          rewrite: (path) => path.replace(/^\/weather/, ""), // 将路径/api替换为空
        },
        "/bgm": {
          target: "https://api.bgm.tv", // 将/bgm访问转换成target
          changeOrigin: true, // 允许跨域
          rewrite: (path) => path.replace(/^\/bgm/, ""), // 将路径/bgm替换为空
        },
        "/api": {
          target: "http://124.223.49.16:8889/api/private/v1", // 将/api访问转换成target
          changeOrigin: true, // 允许跨域
          rewrite: (path) => path.replace(/^\/api/, ""), // 将路径/api替换为空
        },
      }
    },
    // 打包设置
    build: {
      minify: 'terser',
      terserOptions: {
        compress: {
          //生产环境时移除console
          drop_console: true,
          drop_debugger: true,
        },
      },
    },
  }
});
